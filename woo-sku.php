<?php
/* Plugin Name: WooCommerce SKU
 * Plugin URI: https://grabania.pl
 * Description: Wtyczka wyświetlająca SKU produktu w wynikach wyszukiwania i w koszyku. UWAGA: Wtyczka nadpisuje szablon cart/cart.php!
 * Version: 1.0
 * Author: Krzysztof Grabania
 * Author URI: https://grabania.pl
 */

if (!class_exists('Woo_SKU')) {
    class Woo_SKU {
        /**
         * Name of SKU column.
         *
         * @var string
         */
        public static $sku_name = 'Numer katalogowy';

        /**
         * Class constructor.
         */
        public function __construct() {
            add_filter('wc_get_template', [$this, 'replace_cart_template'], 10, 5);
            add_action('woocommerce_after_shop_loop_item_title', [$this, 'add_sku_to_product_loop'], 7);
        }

        /**
         * Override default cart template.
         *
         * @param  string $located
         * @param  string $template_name
         * @param  array $args
         * @param  string $template_path
         * @param  string $default_path
         * @return string
         */
        public function replace_cart_template($located, $template_name, $args, $template_path, $default_path) {
            if ($template_name === 'cart/cart.php') {
                return $this->locate_template($template_name);
            }

            return $located;
        }

        /**
         * Add SKU value to product content in search loop.
         */
        public function add_sku_to_product_loop() {
            global $product;

            if (strlen($product->get_sku()) > 0): ?>
                <span class="woocommerce-sku">
                    <?php printf('%s: %s', static::$sku_name, $product->get_sku());?>
                </span>
            <?php endif;
        }

        /**
         * Change default template path to plugin templates directory.
         *
         * @param  string $template_name
         * @return string
         */
        protected function locate_template($template_name) {
            return plugin_dir_path(__FILE__) . 'templates/' . $template_name;
        }
    }

    new Woo_SKU();
}